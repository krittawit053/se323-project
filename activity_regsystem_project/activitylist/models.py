from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Student(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=50, null=True)
    grades = models.IntegerField(null=True)

    def __str__(self):
        return self.full_name


class Teacher(models.Model):
    #user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.full_name


class Activity(models.Model):
    STATUS = (
            ('Open','Open'),
            ('Close', 'Close'),
            )
    
    organizer = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL)
    student = models.ManyToManyField(Student)
    status = models.CharField(max_length=100, null=True, choices=STATUS)
    name = models.CharField(max_length=30, null=True)
    description = models.TextField(null=True)
    credit = models.IntegerField(null=True)
    maximumStu = models.IntegerField(null=True)
    date_hosted = models.DateTimeField(default=timezone.now, null=True)

    def __str__(self):
        return self.name    
    