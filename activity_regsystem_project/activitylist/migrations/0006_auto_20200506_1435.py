# Generated by Django 3.0.6 on 2020-05-06 07:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('activitylist', '0005_enroll_activity'),
    ]

    operations = [
        migrations.AddField(
            model_name='activity',
            name='organizer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='activitylist.Teacher'),
        ),
        migrations.AddField(
            model_name='activity',
            name='status',
            field=models.CharField(choices=[('Enrolled', 'Enrolled'), ('Not Enrolled', 'Not Enrolled')], max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='student',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='activitylist.Student'),
        ),
        migrations.DeleteModel(
            name='Enroll',
        ),
    ]
