from django.urls import path
from .views import ActListView, ActDetailView, ActScoreListView, ActEnrolledListView
from . import views
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', login_required(views.ActListView), name='activity-list-home'),
    path('activity/<int:pk>/', login_required(ActDetailView.as_view()), name='activity-detail'),
    path('activity-score/', login_required(ActScoreListView.as_view()), name='activity-score'),
    path('activity-enrolled/', login_required(ActEnrolledListView.as_view()), name='activity-enrolled'),
]