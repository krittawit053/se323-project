from django.apps import AppConfig


class ActivitylistConfig(AppConfig):
    name = 'activitylist'
