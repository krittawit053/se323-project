from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import Activity, User, Student

def ActListView(request):
    activities = request.user.student.activity_set.all()
    activities = activities.filter(student=request.user.student.pk)
    print('ACTIVITIES', activities)

    context = {'activities': activities}

    return render(request, 'activitylist/activitylist.html', context)

class ActDetailView(DetailView):
    model = Activity
    template_name = 'activitylist/activity_detail.html' # <app>/<model>_<viewtype>.html

class ActScoreListView(ListView):
    model = Activity
    template_name = 'activitylist/activity_score.html' # <app>/<model>_<viewtype>.html


class ActEnrolledListView(ListView):
    model = Activity
    template_name = 'activitylist/activity_enrolled.html' # <app>/<model>_<viewtype>.html
    queryset = Activity.objects.filter(status='Enrolled')
    context_object_name = 'enrolls'
